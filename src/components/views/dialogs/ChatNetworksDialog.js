/*
Copyright 2020 Dmitry Tyvaniuk <dm0141e@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { _t } from "matrix-react-sdk/src/languageHandler";
import * as sdk from "matrix-react-sdk/src/index";
import { MatrixClientPeg } from "matrix-react-sdk/src/MatrixClientPeg";
import Modal from 'matrix-react-sdk/src/Modal';
import SdkConfig from 'matrix-react-sdk/src/SdkConfig';

const generateIframeUrl = (template, username) => {
    const iframeUrl = template.replace('%(username)', username);
    return iframeUrl[iframeUrl.length - 1] === '/' ? iframeUrl : `${iframeUrl}/`;
};

export default class ChatNetworksDialog extends React.Component {
    static propTypes = {
        onFinished: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        const client = MatrixClientPeg.get();
        // userId is string like as '@username:matrix.org'
        const userId = client.getUserId();
        const username = userId.split(':')[0].slice(1);

        const template = SdkConfig.get().bridges_manager_url;

        if (!template) {
            console.log('URL for Chat Networks not defined in app config');
        }

        this.state = {
            iframeUrl: generateIframeUrl(template, username),
            error: !template,
        };

        this.matixClient = client;
        this.iframeRef = React.createRef();
    }

    componentWillUnmount() {
        this.clearOAthLogic();
    }

    handleIframeLoad = async () => {
        const { iframeUrl } = this.state;
        const token = await this.matixClient.getOpenIdToken();

        this.iframeRef.current.contentWindow.postMessage({
            type: 'login',
            token,
        }, iframeUrl);

        this.initOAuthLogic();
    };

    initOAuthLogic() {
        if (window.todesktop) {
            this.iframeRef.current.contentWindow.postMessage( {
                type: 'use-desktop-login',
            }, this.state.iframeUrl);
            window.addEventListener('message', this.onIframeSendMessage, false);
            window.todesktop.on('custom-finish-oauth-process', this.handleFinishOAuth);
        }
    }

    clearOAthLogic() {
        if (window.todesktop) {
            window.removeEventListener('message', this.onIframeSendMessage);
            window.todesktop.removeListener('custom-finish-oauth-process', this.handleFinishOAuth);
            if (window.todesktop.custom.clearCache) {
                window.todesktop.custom.clearCache();
            }
        }
    }

    handleFinishOAuth = (event, payload) => {
        const { iframeUrl } = this.state;
        this.iframeRef.current.contentWindow.postMessage( {
            type: 'finish-oauth',
            payload,
        }, iframeUrl);
    }

    onIframeSendMessage = (event) => {
        const { data = {} } = event;
        if (data.type !== 'start-oauth') {
            console.log('Unknown postMessage command:', data);
            return;
        }
        window.todesktop.custom.startOAuthProcess(data.payload);
    }

    render() {
        const BaseDialog = sdk.getComponent('views.dialogs.BaseDialog');
        const { error, iframeUrl } = this.state;

        if (error) {
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
            Modal.createTrackedDialog('Cannot open Chat Networks', '', ErrorDialog, {
                title: _t("Cannot open Chat Networks"),
                description: _t("URL for Chat Networks not defined in app config"),
            });
        }

        return (
            <BaseDialog
                className='mx_UserSettingsDialog'
                hasCancel={true}
                onFinished={this.props.onFinished}
                title={_t("Set Up Chat Networks")}
            >
                <div className='ms_SettingsDialog_content' style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <iframe
                        ref={this.iframeRef}
                        onLoad={this.handleIframeLoad}
                        src={iframeUrl}
                        style={{ width: '100%', height: '1400px', border: 'none', margin: '0 auto' }}
                    />
                </div>
            </BaseDialog>
        );
    }
}
