import React from 'react';
import PropTypes from 'prop-types';
import { _t } from 'matrix-react-sdk/src/languageHandler';

const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const DateSeparator = ({ ts }) => {
    const date = new Date(ts);
    const month = monthNames[date.getMonth()];
    const dateLabel = `${_t(month)} ${date.getDate()}`;
    return (
        <h2 className="mx_DateSeparator" role="separator" tabIndex={-1}>
            <div>{ dateLabel }</div>
        </h2>
    );
};

DateSeparator.propTypes = {
    ts: PropTypes.number.isRequired,
};

export default DateSeparator;
