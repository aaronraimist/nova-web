/*
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import dis from 'matrix-react-sdk/src/dispatcher';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import LogoutDialog from "matrix-react-sdk/src/components/views/dialogs/LogoutDialog";
import Modal from "matrix-react-sdk/src/Modal";
import SdkConfig from 'matrix-react-sdk/src/SdkConfig';
import { getHostingLink } from 'matrix-react-sdk/src/utils/HostingLink';
import {MatrixClientPeg} from 'matrix-react-sdk/src/MatrixClientPeg';
import {MenuItem} from "matrix-react-sdk/src/components/structures/ContextMenu";
import * as sdk from "matrix-react-sdk/src/index";

export default class TopLeftMenu extends React.Component {
    static propTypes = {
        displayName: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        onFinished: PropTypes.func,

        // Optional function to collect a reference to the container
        // of this component directly.
        containerRef: PropTypes.func,
    };

    constructor() {
        super();
        this.viewHomePage = this.viewHomePage.bind(this);
        this.openChatNetworks = this.openChatNetworks.bind(this);
        this.openTagManager = this.openTagManager.bind(this);
        this.openSettings = this.openSettings.bind(this);
        this.signIn = this.signIn.bind(this);
        this.signOut = this.signOut.bind(this);
        this.startDirectChat = this.startDirectChat.bind(this);
        this.startGroupChat = this.startGroupChat.bind(this);
    }

    hasHomePage() {
        const config = SdkConfig.get();
        const pagesConfig = config.embeddedPages;
        if (pagesConfig && pagesConfig.homeUrl) {
            return true;
        }
        // This is a deprecated config option for the home page
        // (despite the name, given we also now have a welcome
        // page, which is not the same).
        return !!config.welcomePageUrl;
    }

    render() {
        const isGuest = MatrixClientPeg.get().isGuest();

        const hostingSignupLink = getHostingLink('user-context-menu');
        let hostingSignup = null;
        if (hostingSignupLink) {
            hostingSignup = <div className="mx_TopLeftMenu_upgradeLink">
                {_t(
                    "<a>Upgrade</a> to your own domain", {},
                    {
                        a: sub =>
                            <a href={hostingSignupLink} target="_blank" rel="noreferrer noopener" tabIndex={-1}>{sub}</a>,
                    },
                )}
                <a href={hostingSignupLink} target="_blank" rel="noreferrer noopener" role="presentation" aria-hidden={true} tabIndex={-1}>
                    <img src={require("matrix-react-sdk/res/img/external-link.svg")} width="11" height="10" alt='' />
                </a>
            </div>;
        }

        let homePageItem = null;
        if (this.hasHomePage()) {
            homePageItem = (
                <MenuItem className="mx_TopLeftMenu_icon_home" onClick={this.viewHomePage}>
                    {_t("Home")}
                </MenuItem>
            );
        }

        let signInOutItem;
        if (isGuest) {
            signInOutItem = (
                <MenuItem className="mx_TopLeftMenu_icon_signin" onClick={this.signIn}>
                    {_t("Sign in")}
                </MenuItem>
            );
        } else {
            signInOutItem = (
                <MenuItem className="mx_TopLeftMenu_icon_signout" onClick={this.signOut}>
                    {_t("Sign out")}
                </MenuItem>
            );
        }

        const helpItem = (
            <MenuItem className="mx_TopLeftMenu_icon_help" onClick={this.openHelp}>
                {_t("Help")}
            </MenuItem>
        );

        const chatNetworksItem = (
            <MenuItem className="mx_TopLeftMenu_icon_settings" onClick={this.openChatNetworks}>
                {_t("Set Up Chat Networks")}
            </MenuItem>
        );

        const tagManagerItem = (
            <MenuItem className="mx_TopLeftMenu_icon_settings" onClick={this.openTagManager}>
                {_t("Tag Manager")}
            </MenuItem>
        );

        const settingsItem = (
            <MenuItem className="mx_TopLeftMenu_icon_settings" onClick={this.openSettings}>
                {_t("Settings")}
            </MenuItem>
        );

        const startDirectChatItem = (
          <MenuItem className="mx_TopLeftMenu_icon_settings" onClick={this.startDirectChat}>
              {_t("New Direct Chat")}
          </MenuItem>
        );

        const startGroupChatItem = (
            <MenuItem className="mx_TopLeftMenu_icon_settings" onClick={this.startGroupChat}>
                {_t("New Group")}
            </MenuItem>
        );

        return <div className="mx_TopLeftMenu" ref={this.props.containerRef} role="menu">
            <div className="mx_TopLeftMenu_section_noIcon" aria-readonly={true} tabIndex={-1}>
                <div>{this.props.displayName}</div>
                <div className="mx_TopLeftMenu_greyedText" aria-hidden={true}>{this.props.userId}</div>
                {hostingSignup}
            </div>
            <ul className="mx_TopLeftMenu_section_withIcon" role="none">
                {startDirectChatItem}
                {startGroupChatItem}
                {homePageItem}
                {chatNetworksItem}
                {tagManagerItem}
                {settingsItem}
                {helpItem}
                {signInOutItem}
            </ul>
        </div>;
    }

    openHelp = () => {
        this.closeMenu();
        const RedesignFeedbackDialog = sdk.getComponent("views.dialogs.RedesignFeedbackDialog");
        Modal.createTrackedDialog('Report bugs & give feedback', '', RedesignFeedbackDialog);
    };

    viewHomePage() {
        dis.dispatch({action: 'view_home_page'});
        this.closeMenu();
    }

    openChatNetworks() {
        dis.dispatch({action: 'view_chat_networks'});
        this.closeMenu();
    }

    openTagManager() {
        dis.dispatch({action: 'view_tag_manager'});
        this.closeMenu();
    }

    openSettings() {
        dis.dispatch({action: 'view_user_settings'});
        this.closeMenu();
    }

    signIn() {
        dis.dispatch({action: 'start_login'});
        this.closeMenu();
    }

    signOut() {
        Modal.createTrackedDialog('Logout E2E Export', '', LogoutDialog);
        this.closeMenu();
    }

    closeMenu() {
        if (this.props.onFinished) this.props.onFinished();
    }

    startDirectChat() {
        dis.dispatch({action: 'view_create_chat'});
        this.closeMenu();
    }

    startGroupChat() {
        dis.dispatch({action: 'view_create_room'});
        this.closeMenu();
    }
}
