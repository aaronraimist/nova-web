/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import React from 'react';
import PropTypes from 'prop-types';
import dis from 'matrix-react-sdk/src/dispatcher';
import { containsEmote } from 'matrix-react-sdk/src/editor/serialize';
import RoomViewStore from 'matrix-react-sdk/src/stores/RoomViewStore';
import * as sdk from 'matrix-react-sdk/src/index';
import Modal from 'matrix-react-sdk/src/Modal';
import {_t} from 'matrix-react-sdk/src/languageHandler';
import MatrixClientContext from "matrix-react-sdk/src/contexts/MatrixClientContext";
// eslint-disable-next-line max-len
import SendMessageComposer, { createMessageContent } from 'matrix-react-sdk/src/components/views/rooms/SendMessageComposer';
import BasicMessageComposer from "matrix-react-sdk/src/components/views/rooms/BasicMessageComposer";

export default class NovaSendMessageComposer extends SendMessageComposer {
    static replaces = 'SendMessageComposer';

    static propTypes = {
        room: PropTypes.object.isRequired,
        placeholder: PropTypes.string,
        jumpToBottom: PropTypes.func,
        permalinkCreator: PropTypes.object.isRequired,
    };

    static contextType = MatrixClientContext;

    constructor(props) {
        super(props);
    }

    async _sendMessage() {
        if (this.model.isEmpty) {
            return;
        }

        let shouldSend = true;

        if (!containsEmote(this.model) && this._isSlashCommand()) {
            const [cmd, commandText] = this._getSlashCommand();
            if (cmd) {
                shouldSend = false;
                this._runSlashCommand(cmd);
            } else {
                // ask the user if their unknown command should be sent as a message
                const QuestionDialog = sdk.getComponent("dialogs.QuestionDialog");
                const {finished} = Modal.createTrackedDialog("Unknown command", "", QuestionDialog, {
                    title: _t("Unknown Command"),
                    description: <div>
                        <p>
                            { _t("Unrecognised command: %(commandText)s", {commandText}) }
                        </p>
                        <p>
                            { _t("You can use <code>/help</code> to list available commands. " +
                                "Did you mean to send this as a message?", {}, {
                                code: t => <code>{ t }</code>,
                            }) }
                        </p>
                        <p>
                            { _t("Hint: Begin your message with <code>//</code> to start it with a slash.", {}, {
                                code: t => <code>{ t }</code>,
                            }) }
                        </p>
                    </div>,
                    button: _t('Send as message'),
                });
                const [sendAnyway] = await finished;
                // if !sendAnyway bail to let the user edit the composer and try again
                if (!sendAnyway) return;
            }
        }

        if (shouldSend) {
            const isReply = !!RoomViewStore.getQuotingEvent();
            const {roomId} = this.props.room;
            const content = createMessageContent(this.model, this.props.permalinkCreator);
            this.context.sendMessage(roomId, content);

            dis.dispatch({
                action: 'track_event',
                message: 'Send message',
            });

            if (isReply) {
                // Clear reply_to_event as we put the message into the queue
                // if the send fails, retry will handle resending.
                dis.dispatch({
                    action: 'reply_to_event',
                    event: null,
                });
            }
        }

        this.sendHistoryManager.save(this.model);
        // jump to bottom
        this.props.jumpToBottom();
        // clear composer
        this.model.reset([]);
        this._editorRef.clearUndoHistory();
        this._editorRef.focus();
        this._clearStoredEditorState();
    }
    render() {
        return (
            <div className="mx_SendMessageComposer" onClick={this.focusComposer} onKeyDown={this._onKeyDown}>
                <div className="mx_SendMessageComposer_overlayWrapper">
                </div>
                <BasicMessageComposer
                    ref={this._setEditorRef}
                    model={this.model}
                    room={this.props.room}
                    label={this.props.placeholder}
                    placeholder={this.props.placeholder}
                    onChange={this._saveStoredEditorState}
                />
            </div>
        );
    }
}
