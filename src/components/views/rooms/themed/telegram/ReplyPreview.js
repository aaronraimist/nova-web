/*
Copyright 2017 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import dis from 'matrix-react-sdk/src/dispatcher';
import * as sdk from 'matrix-react-sdk/src';
import RoomViewStore from 'matrix-react-sdk/src/stores/RoomViewStore';
import PropTypes from "prop-types";
import {RoomPermalinkCreator} from "matrix-react-sdk/src/utils/permalinks/Permalinks";
import { MatrixClientPeg } from "matrix-react-sdk/src/MatrixClientPeg";
import { getReplyContentBody } from '../../../../../utils/replyEvent';

function cancelQuoting() {
    dis.dispatch({
        action: 'reply_to_event',
        event: null,
    });
}

export default class ThemedReplyPreview extends React.Component {
    static propTypes = {
        permalinkCreator: PropTypes.instanceOf(RoomPermalinkCreator).isRequired,
    };

    constructor(props) {
        super(props);
        this.unmounted = false;

        this.state = {
            event: RoomViewStore.getQuotingEvent(),
        };

        this._onRoomViewStoreUpdate = this._onRoomViewStoreUpdate.bind(this);
        this._roomStoreToken = RoomViewStore.addListener(this._onRoomViewStoreUpdate);
    }

    componentWillUnmount() {
        this.unmounted = true;

        // Remove RoomStore listener
        if (this._roomStoreToken) {
            this._roomStoreToken.remove();
        }
    }

    _onRoomViewStoreUpdate() {
        if (this.unmounted) return;

        const event = RoomViewStore.getQuotingEvent();
        if (this.state.event !== event) {
            this.setState({ event });
        }
    }

    _getReplyContent() {
        const { event } = this.state;
        if (event.getType() !== 'm.room.message') return null;
        const eventContent = event.getContent();
        const { msgtype, info = {} } = eventContent;
        const SenderProfile = sdk.getComponent('messages.SenderProfile');

        let imagePreview = null;
        if (msgtype === 'm.image') {
            const pixelRatio = window.devicePixelRatio;
            const thumbWidth = Math.round(800 * pixelRatio);
            const thumbHeight = Math.round(600 * pixelRatio);
            const src = MatrixClientPeg.get().mxcUrlToHttp(info.thumbnail_url, thumbWidth, thumbHeight);
            imagePreview = (
                <div className="mx_ReplyPreview_imagePreview">
                    <img src={src} width={36} height={36} />
                </div>
            );
        }
        return (
            <div className="mx_ReplyPreview_wrapper">
                {imagePreview}
                <div className="mx_ReplyPreview_content">
                    <SenderProfile mxEvent={event} enableFlair={true} />
                    <span className="mx_ReplyPreview_contentText">{getReplyContentBody(eventContent)}</span>
                </div>
            </div>
        );
    }

    render() {
        if (!this.state.event) return null;

        return <div className="mx_ReplyPreview">
            <div className="mx_ReplyPreview_section">
                {this._getReplyContent()}
                <div className="mx_ReplyPreview_header mx_ReplyPreview_cancel">
                    <img className="mx_filterFlipColor" src={require("matrix-react-sdk/res/themes/telegram/img/icon-close-dialog.svg")} onClick={cancelQuoting} />
                </div>
            </div>
        </div>;
    }
}
