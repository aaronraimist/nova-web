/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 Vector Creations Ltd
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import SettingsStore from "matrix-react-sdk/src/settings/SettingsStore";
import Timer from "matrix-react-sdk/src/utils/Timer";
import React from "react";
import classNames from 'classnames';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import {MatrixClientPeg} from "matrix-react-sdk/src/MatrixClientPeg";
// eslint-disable-next-line camelcase
import rate_limited_func from "matrix-react-sdk/src/ratelimitedfunc";
import * as Rooms from 'matrix-react-sdk/src/Rooms';
import DMRoomMap from 'matrix-react-sdk/src/utils/DMRoomMap';
import TagOrderStore from 'matrix-react-sdk/src/stores/TagOrderStore';
import RoomListStore, {TAG_DM} from 'matrix-react-sdk/src/stores/RoomListStore';
import CustomRoomTagStore from 'matrix-react-sdk/src/stores/CustomRoomTagStore';
import RoomSearchStore from '../../../../../stores/RoomSearchStore';
import GroupStore from 'matrix-react-sdk/src/stores/GroupStore';
import * as sdk from "matrix-react-sdk/src/index";
import * as Receipt from "matrix-react-sdk/src/utils/Receipt";
import {RovingTabIndexProvider} from "matrix-react-sdk/src/accessibility/RovingTabIndex";
import * as RoomNotifs from 'matrix-react-sdk/src/RoomNotifs';
import dis from "matrix-react-sdk/src/dispatcher";
import RoomViewStore from "matrix-react-sdk/src/stores/RoomViewStore";
import * as Unread from "matrix-react-sdk/src/Unread";

const HIDE_CONFERENCE_CHANS = true;
const STANDARD_TAGS_REGEX = /^(m\.(favourite|lowpriority|server_notice)|im\.vector\.fake\.(invite|recent|direct|archived))$/;
const HOVER_MOVE_TIMEOUT = 1000;

const TILE_HEIGHT = 62;

const RoomList = createReactClass({
    displayName: 'RoomList',

    propTypes: {
        ConferenceHandler: PropTypes.any,
        searchFilter: PropTypes.string,
    },

    getInitialState: function() {
        this._hoverClearTimer = null;
        this._subListRefs = {
            // key => RoomSubList ref
        };

        this._needUpdateSizesTemp = true;
        this._needUpdateSizes = false;

        this._checkSubListsOverflow();

        return {
            isLoadingLeftRooms: false,
            totalRoomCount: null,
            lists: {},
            incomingCallTag: null,
            incomingCall: null,
            selectedTags: [],
            hover: false,
            scrollerHeight: 800,
            customTags: CustomRoomTagStore.getTags(),
            archivedOpen: false,
        };
    },

    componentWillMount: function() {
        this.mounted = false;

        const cli = MatrixClientPeg.get();

        cli.on("Room", this.onRoom);
        cli.on("deleteRoom", this.onDeleteRoom);
        cli.on("Room.receipt", this.onRoomReceipt);
        cli.on("RoomMember.name", this.onRoomMemberName);
        cli.on("Event.decrypted", this.onEventDecrypted);
        cli.on("accountData", this.onAccountData);
        cli.on("Group.myMembership", this._onGroupMyMembership);
        cli.on("RoomState.events", this.onRoomStateEvents);

        const dmRoomMap = DMRoomMap.shared();
        // A map between tags which are group IDs and the room IDs of rooms that should be kept
        // in the room list when filtering by that tag.
        this._visibleRoomsForGroup = {
            // $groupId: [$roomId1, $roomId2, ...],
        };
        // All rooms that should be kept in the room list when filtering.
        // By default, show all rooms.
        this._visibleRooms = MatrixClientPeg.get().getVisibleRooms();

        // Listen to updates to group data. RoomList cares about members and rooms in order
        // to filter the room list when group tags are selected.
        this._groupStoreToken = GroupStore.registerListener(null, () => {
            (TagOrderStore.getOrderedTags() || []).forEach((tag) => {
                if (tag[0] !== '+') {
                    return;
                }
                // This group's rooms or members may have updated, update rooms for its tag
                this.updateVisibleRoomsForTag(dmRoomMap, tag);
                this.updateVisibleRooms();
            });
        });

        this._tagStoreToken = TagOrderStore.addListener(() => {
            // Filters themselves have changed
            this.updateVisibleRooms();
        });

        this._roomListStoreToken = RoomListStore.addListener(() => {
            this._delayedRefreshRoomList();
        });


        if (SettingsStore.isFeatureEnabled("feature_custom_tags")) {
            this._customTagStoreToken = CustomRoomTagStore.addListener(() => {
                this.setState({
                    customTags: CustomRoomTagStore.getTags(),
                });
            });
        }

        this.refreshRoomList();
    },

    componentDidMount: function() {
        this.mounted = true;
        this.dispatcherRef = dis.register(this.onAction);
        this._checkSubListsOverflow();

        if (this.props.resizeNotifier) {
            this.props.resizeNotifier.on("leftPanelResized", this.onResize);
        }
        this.onResize();
    },

    componentDidUpdate: function() {
        this._checkSubListsOverflow();
    },

    componentWillUnmount: function() {
        this.mounted = false;

        dis.unregister(this.dispatcherRef);
        if (MatrixClientPeg.get()) {
            MatrixClientPeg.get().removeListener("Room", this.onRoom);
            MatrixClientPeg.get().removeListener("deleteRoom", this.onDeleteRoom);
            MatrixClientPeg.get().removeListener("Room.receipt", this.onRoomReceipt);
            MatrixClientPeg.get().removeListener("RoomMember.name", this.onRoomMemberName);
            MatrixClientPeg.get().removeListener("Event.decrypted", this.onEventDecrypted);
            MatrixClientPeg.get().removeListener("accountData", this.onAccountData);
            MatrixClientPeg.get().removeListener("Group.myMembership", this._onGroupMyMembership);
            MatrixClientPeg.get().removeListener("RoomState.events", this.onRoomStateEvents);
        }

        if (this.props.resizeNotifier) {
            this.props.resizeNotifier.removeListener("leftPanelResized", this.onResize);
        }

        if (this._tagStoreToken) {
            this._tagStoreToken.remove();
        }

        if (this._roomListStoreToken) {
            this._roomListStoreToken.remove();
        }
        if (this._customTagStoreToken) {
            this._customTagStoreToken.remove();
        }

        // NB: GroupStore is not a Flux.Store
        if (this._groupStoreToken) {
            this._groupStoreToken.unregister();
        }

        // cancel any pending calls to the rate_limited_funcs
        this._delayedRefreshRoomList.cancelPendingCall();
    },

    onAction: function(payload) {
        switch (payload.action) {
            // TODO Move lists logic to store
            case 'view_room_delta': {
                if (this.props.searchFilter || this.state.archivedOpen) return;
                const currentRoomId = RoomViewStore.getRoomId();

                const { lists } = this.state;
                const {
                    "im.vector.fake.invite": inviteRooms,
                    "m.favourite": favouriteRooms,
                    [TAG_DM]: dmRooms,
                    "im.vector.fake.recent": recentRooms,
                    // "m.lowpriority": lowPriorityRooms,
                    // "im.vector.fake.archived": historicalRooms,
                    // "m.server_notice": serverNoticeRooms,
                    ...tags
                } = lists;

                let rooms;
                let pinnedRooms = [];
                // if (archivedOpen) {
                //     rooms = lowPriorityRooms.concat(historicalRooms);
                // } else {
                    const shownCustomTagRooms = Object.keys(tags)
                        .filter(tagName => {
                            return (!this.state.customTags || this.state.customTags[tagName]) &&
                                !tagName.match(STANDARD_TAGS_REGEX);
                            })
                        .map(tagName => tags[tagName]);

                    rooms = [
                        ...dmRooms,
                        ...recentRooms,
                        // ...serverNoticeRooms,
                        // eslint-disable-next-line prefer-spread
                        ...[].concat.apply([], shownCustomTagRooms),
                    ];
                    pinnedRooms = [...inviteRooms, ...favouriteRooms];
                // }

                if (payload.unread) {
                    // filter to only notification rooms (and our current active room so we can index properly)
                    rooms = rooms.filter(room => {
                        return Unread.doesRoomHaveUnreadMessages(room);
                    });
                    pinnedRooms = pinnedRooms.filter(room => {
                        return Unread.doesRoomHaveUnreadMessages(room);
                    });
                }

                const sortedRooms = [...pinnedRooms, ...this._sortRoomListByRecent(rooms)];

                const currentIndex = sortedRooms.findIndex(room => room.roomId === currentRoomId);

                const moveToStart = currentIndex === -1 || (currentIndex === sortedRooms.length - 1 && payload.delta === 1);
                const moveToEnd = currentIndex === 0 && payload.delta === -1;

                let room;
                if (moveToEnd) {
                    room = sortedRooms[sortedRooms.length - 1];
                } else {
                    room = moveToStart ? sortedRooms[0] : sortedRooms[currentIndex + payload.delta];
                }

                if (room) {
                    dis.dispatch({
                        action: 'view_room',
                        room_id: room.roomId,
                        show_room_tile: true, // to make sure the room gets scrolled into view
                    });
                }
                break;
            }
        }
    },

    onResize: function() {
        if (this.mounted && this.resizeContainer) {
            this.setState({
                scrollerHeight: this.resizeContainer.offsetHeight,
            });
        }
    },

    onRoom: function(room) {
        this.updateVisibleRooms();
    },

    onRoomStateEvents: function(ev, state) {
        if (ev.getType() === "m.room.create" || ev.getType() === "m.room.tombstone") {
            this.updateVisibleRooms();
        }
    },

    onDeleteRoom: function(roomId) {
        this.updateVisibleRooms();
    },

    onArchivedHeaderClick: function(isHidden, scrollToPosition) {
        if (!isHidden) {
            const self = this;
            this.setState({ isLoadingLeftRooms: true });
            // we don't care about the response since it comes down via "Room"
            // events.
            MatrixClientPeg.get().syncLeftRooms().catch(function(err) {
                console.error("Failed to sync left rooms: %s", err);
                console.error(err);
            }).finally(function() {
                self.setState({ isLoadingLeftRooms: false });
            });
        }
    },

    onRoomReceipt: function(receiptEvent, room) {
        // because if we read a notification, it will affect notification count
        // only bother updating if there's a receipt from us
        if (Receipt.findReadReceiptFromUserId(receiptEvent, MatrixClientPeg.get().credentials.userId)) {
            this._delayedRefreshRoomList();
        }
    },

    onRoomMemberName: function(ev, member) {
        this._delayedRefreshRoomList();
    },

    onEventDecrypted: function(ev) {
        // An event being decrypted may mean we need to re-order the room list
        this._delayedRefreshRoomList();
    },

    onAccountData: function(ev) {
        if (ev.getType() == 'm.direct') {
            this._delayedRefreshRoomList();
        }
    },

    _onGroupMyMembership: function(group) {
        this.forceUpdate();
    },

    onMouseMove: async function(ev) {
        if (!this._hoverClearTimer) {
            this.setState({hover: true});
            this._hoverClearTimer = new Timer(HOVER_MOVE_TIMEOUT);
            this._hoverClearTimer.start();
            let finished = true;
            try {
                await this._hoverClearTimer.finished();
            } catch (err) {
                finished = false;
            }
            this._hoverClearTimer = null;
            if (finished) {
                this.setState({hover: false});
                this._delayedRefreshRoomList();
            }
        } else {
            this._hoverClearTimer.restart();
        }
    },

    onMouseLeave: function(ev) {
        if (this._hoverClearTimer) {
            this._hoverClearTimer.abort();
            this._hoverClearTimer = null;
        }
        this.setState({hover: false});

        // Refresh the room list just in case the user missed something.
        this._delayedRefreshRoomList();
    },

    _delayedRefreshRoomList: rate_limited_func(function() {
        // eslint-disable-next-line babel/no-invalid-this
        this.refreshRoomList();
    }, 500),

    // Update which rooms and users should appear in RoomList for a given group tag
    updateVisibleRoomsForTag: function(dmRoomMap, tag) {
        if (!this.mounted) return;
        // For now, only handle group tags
        if (tag[0] !== '+') return;

        this._visibleRoomsForGroup[tag] = [];
        GroupStore.getGroupRooms(tag).forEach((room) => this._visibleRoomsForGroup[tag].push(room.roomId));
        GroupStore.getGroupMembers(tag).forEach((member) => {
            if (member.userId === MatrixClientPeg.get().credentials.userId) return;
            dmRoomMap.getDMRoomsForUserId(member.userId).forEach(
                (roomId) => this._visibleRoomsForGroup[tag].push(roomId),
            );
        });
        // TODO: Check if room has been tagged to the group by the user
    },

    // Update which rooms and users should appear according to which tags are selected
    updateVisibleRooms: function() {
        const selectedTags = TagOrderStore.getSelectedTags();
        const visibleGroupRooms = [];

        selectedTags.forEach((tag) => {
            (this._visibleRoomsForGroup[tag] || []).forEach(
                (roomId) => visibleGroupRooms.push(roomId),
            );
        });

        // If there are any tags selected, constrain the rooms listed to the
        // visible rooms as determined by visibleGroupRooms. Here, we
        // de-duplicate and filter out rooms that the client doesn't know
        // about (hence the Set and the null-guard on `room`).
        if (selectedTags.length > 0) {
            const roomSet = new Set();
            visibleGroupRooms.forEach((roomId) => {
                const room = MatrixClientPeg.get().getRoom(roomId);
                if (room) {
                    roomSet.add(room);
                }
            });
            this._visibleRooms = Array.from(roomSet);
        } else {
            // Show all rooms
            this._visibleRooms = MatrixClientPeg.get().getVisibleRooms();
        }
        this._delayedRefreshRoomList();
    },

    refreshRoomList: function() {
        if (this.state.hover) {
            // Don't re-sort the list if we're hovering over the list
            return;
        }

        // TODO: ideally we'd calculate this once at start, and then maintain
        // any changes to it incrementally, updating the appropriate sublists
        // as needed.
        // Alternatively we'd do something magical with Immutable.js or similar.
        const lists = this.getRoomLists();
        let totalRooms = 0;
        for (const l of Object.values(lists)) {
            totalRooms += l.length;
        }
        this.setState({
            lists,
            totalRoomCount: totalRooms,
            // Do this here so as to not render every time the selected tags
            // themselves change.
            selectedTags: TagOrderStore.getSelectedTags(),
        }, () => {
            // we don't need to restore any size here, do we?
            // i guess we could have triggered a new group to appear
            // that already an explicit size the last time it appeared ...
            this._checkSubListsOverflow();
        });
    },

    getTagNameForRoomId: function(roomId) {
        const lists = RoomListStore.getRoomLists();
        for (const tagName of Object.keys(lists)) {
            for (const room of lists[tagName]) {
                // Should be impossible, but guard anyways.
                if (!room) {
                    continue;
                }
                const myUserId = MatrixClientPeg.get().getUserId();
                if (HIDE_CONFERENCE_CHANS && Rooms.isConfCallRoom(room, myUserId, this.props.ConferenceHandler)) {
                    continue;
                }

                if (room.roomId === roomId) return tagName;
            }
        }

        return null;
    },

    getRoomLists: function() {
        const lists = RoomListStore.getRoomLists();

        const filteredLists = {};

        const isRoomVisible = {
            // $roomId: true,
        };

        this._visibleRooms.forEach((r) => {
            isRoomVisible[r.roomId] = true;
        });

        Object.keys(lists).forEach((tagName) => {
            const filteredRooms = lists[tagName].filter((taggedRoom) => {
                // Somewhat impossible, but guard against it anyway
                if (!taggedRoom) {
                    return;
                }
                const myUserId = MatrixClientPeg.get().getUserId();
                if (HIDE_CONFERENCE_CHANS && Rooms.isConfCallRoom(taggedRoom, myUserId, this.props.ConferenceHandler)) {
                    return;
                }

                return Boolean(isRoomVisible[taggedRoom.roomId]);
            });

            if (filteredRooms.length > 0 || tagName.match(STANDARD_TAGS_REGEX)) {
                filteredLists[tagName] = filteredRooms;
            }
        });

        return filteredLists;
    },

    _hideTooltip: function(e) {
        // Hide tooltip when scrolling, as we'll no longer be over the one we were on
        if (this.tooltip && this.tooltip.style.display !== "none") {
            this.tooltip.style.display = "none";
        }
    },

    _makeGroupInviteTiles(filter) {
        const ret = [];
        const lcFilter = filter && filter.toLowerCase();

        const GroupInviteTile = sdk.getComponent('groups.GroupInviteTile');
        for (const group of MatrixClientPeg.get().getGroups()) {
            const {groupId, name, myMembership} = group;
            // filter to only groups in invite state and group_id starts with filter or group name includes it
            if (myMembership !== 'invite') continue;
            if (lcFilter && !groupId.toLowerCase().startsWith(lcFilter) &&
                !(name && name.toLowerCase().includes(lcFilter))) continue;
            ret.push(<GroupInviteTile key={groupId} group={group} collapsed={this.props.collapsed} />);
        }

        return ret;
    },

    // check overflow for scroll indicator gradient
    _checkSubListsOverflow() {
        Object.values(this._subListRefs).forEach(l => l.checkOverflow());
    },

    _subListRef: function(key, ref) {
        if (!ref) {
            delete this._subListRefs[key];
        } else {
            this._subListRefs[key] = ref;
        }
    },

    _getLastMessageEventTimeline: function({ timeline }) {
        let lastMessageTs = 0;
        for (let i = timeline.length - 1; i >= 0; i--) {
            if (timeline[i].event.type) {
                lastMessageTs = timeline[i].event.origin_server_ts;
                break;
            }
        }
        return lastMessageTs;
    },

    _sortRoomListByRecent: function(list) {
        return list.sort((a, b) => {
            return this._getLastMessageEventTimeline(b) - this._getLastMessageEventTimeline(a);
        });
    },

    _buildSubList: function(props) {
        const defaultProps = {
            forceExpand: !!this.props.searchFilter,
            itemHeight: TILE_HEIGHT,
            scrollerHeight: props.scrollerHeight || this.state.scrollerHeight,
        };

        const {key, label, nestedList, ...otherProps} = props || {};
        const chosenKey = key || label;

        const RoomSubList = sdk.getComponent('structures.RoomSubList');

        return (
            <RoomSubList
                ref={this._subListRef.bind(this, chosenKey)}
                key={chosenKey}
                label={label}
                {...defaultProps}
                {...otherProps}
                nestedList={(refreshSubList) => (
                    <>
                        {nestedList && nestedList.map(comp => React.cloneElement(comp, {refreshSubList: refreshSubList}))}
                    </>
                )}
            />
        );
    },

    onArchivedOpenClick: function(ev) {
        this.setState({ archivedOpen: true });
    },

    onArchivedCloseClick: function(ev) {
        this.setState({ archivedOpen: false });
    },

    getArchivedButton: function() {
        const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
        const { lists } = this.state;
        const roomNames = lists['m.lowpriority'].slice(0, 5).map(room => room.name);
        const notificationsCount = RoomNotifs.aggregateNotificationCount(lists['m.lowpriority']).count;
        return (
            <AccessibleButton
                key="archivedButton"
                className="nv_RoomList_archivedBtn mx_RoomTile"
                onClick={this.onArchivedOpenClick}
            >
                <div className="nv_RoomList_archivedBtn_icon" />
                <div className="mx_RoomTile_content">
                    <div className="mx_RoomTile_labelContainer">
                        <div className="mx_RoomTile_name">Archived chats</div>
                        <div className="mx_RoomTile_eventPreview">
                            {roomNames.join(', ')}
                        </div>
                    </div>
                    <div className="mx_RoomTile_statusGroup" />
                </div>
                <div className="mx_RoomTile_notifications">
                    {notificationsCount > 0 && <div className="mx_RoomTile_badge">{notificationsCount}</div>}
                </div>
            </AccessibleButton>
        );
    },

    renderArchivedSubList: function() {
        const { lists, archivedOpen } = this.state;
        if (!lists['m.lowpriority'].length && !lists['im.vector.fake.archived'].length) {
            return null;
        }
        const archivedList = lists['m.lowpriority'].concat(lists['im.vector.fake.archived']);
        const props = {
            key: 'archived-list',
            list: this._sortRoomListByRecent(archivedList),
        };

        const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
        const header = (
            <AccessibleButton
                className="nv_RoomList_RoomList_archived_header"
                onClick={this.onArchivedCloseClick}
            >
                    <div className="nv_RoomList_RoomList_archived_headerIcon" />
                    <div className="nv_RoomList_RoomList_archived_headerLabel">
                        {_t('Archived chats')}
                    </div>
            </AccessibleButton>
        );
        const archivedClassName = classNames({
            'nv_RoomList_RoomList_archived': true,
            'nv_RoomList_RoomList_archivedVisible': archivedOpen,
        });
        return (
            <>
                <div className={`mx_RoomList_archivedShadow ${archivedOpen ? 'mx_RoomList_archivedShadowVisible' : ''}`} />
                <div className={archivedClassName}>
                    {header}
                    {this._buildSubList(props)}
                </div>
            </>
        );
    },

    renderMainSubList: function() {
        const RoomTileGroup = sdk.getComponent('rooms.RoomTileGroup');

        const { lists } = this.state;

        const invites = lists['im.vector.fake.invite'];
        const favourites = lists['m.favourite'];

        const ignoredArr = ['im.vector.fake.invite', 'm.favourite', 'm.lowpriority', 'im.vector.fake.archived'];

        const mainList = Object.keys(lists).reduce((acc, key) => {
            return ignoredArr.includes(key) ? acc : acc.concat(lists[key]);
         }, []);

        const archivedCount = lists['m.lowpriority'].length;

        const nestedList = [
            <React.Fragment key={'Community Invites'}>
                {this._makeGroupInviteTiles()}
            </React.Fragment>,
            <RoomTileGroup
                key={'im.vector.fake.invite'}
                list={invites}
                draggable={false}
                isInvite={true}
                pinned={false}
                itemHeight={TILE_HEIGHT}
            />,
            <RoomTileGroup
                key={'m.favourite'}
                list={favourites}
                draggable={true}
                isInvite={false}
                pinned={true}
                itemHeight={TILE_HEIGHT}
            />,
        ];

        const props = {
            key: 'main-list',
            nestedListHeight: (invites.length + favourites.length) * TILE_HEIGHT,
            nestedList,
            list: this._sortRoomListByRecent(mainList),
            scrollerHeight: archivedCount ? this.state.scrollerHeight - TILE_HEIGHT : this.state.scrollerHeight,
        };

        return (<>
            {this._buildSubList(props)}
            {archivedCount && this.getArchivedButton()}
        </>);
    },

    renderSearchSubList: function() {
        const RoomTileGroup = sdk.getComponent('rooms.RoomTileGroup');
        const roomsIdsBySearch = RoomSearchStore.searchRooms(this.props.searchFilter);

        const { lists } = this.state;

        const {
            'm.lowpriority': lowpriorityList,
            'im.vector.fake.archived': archivedList,
            ...restLists
        } = lists;

        const mainRooms = Object.keys(restLists)
            .reduce((acc, key) => {
                const filteredRooms = restLists[key].filter(({ roomId }) => roomsIdsBySearch.includes(roomId));
                return acc.concat(filteredRooms);
            }, []);

        const archivedRooms = lowpriorityList.concat(archivedList)
            .filter(({ roomId }) => roomsIdsBySearch.includes(roomId));

        const nestedList = [
            <RoomTileGroup
                key={'main'}
                list={this._sortRoomListByRecent(mainRooms)}
                itemHeight={TILE_HEIGHT}
            />,
            <RoomTileGroup
                key={'archived'}
                list={this._sortRoomListByRecent(archivedRooms)}
                label="Archived chats"
                itemHeight={TILE_HEIGHT}
            />,
        ];

        const props = {
            key: 'search-list',
            list: [],
            nestedList,
        };

        return this._buildSubList(props);
    },

    _collectResizeContainer: function(el) {
        this.resizeContainer = el;
    },

    render: function() {
        const {resizeNotifier, collapsed, searchFilter, ConferenceHandler, onKeyDown, ...props} = this.props; // eslint-disable-line

        let content;
        if (this.props.searchFilter) {
            content = this.renderSearchSubList();
        } else {
            content = (
                <>
                    {this.renderMainSubList()}
                    {this.renderArchivedSubList()}
                </>
            );
        }
        return (
            <RovingTabIndexProvider handleHomeEnd={true} onKeyDown={onKeyDown}>
                {({onKeyDownHandler}) => <div
                    {...props}
                    onKeyDown={onKeyDownHandler}
                    ref={this._collectResizeContainer}
                    className="mx_RoomList"
                    role="tree"
                    aria-label={_t("Rooms")}
                    onMouseMove={this.onMouseMove}
                    onMouseLeave={this.onMouseLeave}
                >
                    {content}
                </div> }
            </RovingTabIndexProvider>
        );
    },
});

export default RoomList;
