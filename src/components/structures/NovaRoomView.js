/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2018, 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// TODO: This component is enormous! There's several things which could stand-alone:
//  - Search results component
//  - Drag and drop

import React, {createRef} from 'react';
// import PropTypes from 'prop-types';
import classNames from 'classnames';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import * as sdk from 'matrix-react-sdk/src/index';
import { isOnlyCtrlOrCmdKeyEvent, Key } from 'matrix-react-sdk/src/Keyboard';
import ContentMessages from 'matrix-react-sdk/src/ContentMessages';
import SettingsStore from "matrix-react-sdk/src/settings/SettingsStore";
import RoomContext from "matrix-react-sdk/src/contexts/RoomContext";

import MainSplit from 'matrix-react-sdk/src/components/structures/MainSplit';
import RightPanel from 'matrix-react-sdk/src/components/structures/RightPanel';
import RoomView from 'matrix-react-sdk/src/components/structures/RoomView';
import AccessibleButton from "matrix-react-sdk/src/components/views/elements/AccessibleButton";

import { checkIsDirectChat, checkIsBridgedChat } from '../../utils/roomIdentifyUtils';

export default class NovaRoomView extends RoomView {
    static replaces = 'RoomView';

    static propTypes = {
        ...super.propTypes,
    };

    static defaultProps = {
        ...super.defaultProps,
    }

    constructor(props, context) {
        super(props, context);

        this.state = {
            ...this.state,
            directChat: false,
            isBridged: false,
            useStraitLayout: false,
        };
    }

    UNSAFE_componentWillMount = () => {
        this._timelineWidthWatcher = createRef();

        super.UNSAFE_componentWillMount();
    }

    onReactKeyDown = (ev) => {
        const ctrlCmdOnly = isOnlyCtrlOrCmdKeyEvent(ev);

        let handled = false;
        switch (ev.key) {
            case Key.F:
                if (ctrlCmdOnly) {
                    this.onSearchClick();
                    handled = true;
                }
                break;
        }

        if (handled) {
            ev.stopPropagation();
            ev.preventDefault();
        } else {
            super.onReactKeyDown(ev);
        }
    }

    // called when state.room is first initialised (either at initial load,
    // after a successful peek, or after we join the room).
    _onRoomLoaded = (room) => {
        this._checkIsDirectChat(room);
        this._checkIsBridgedChat(room);

        super._onRoomLoaded(room);
    }

    onResize = () => {
        this._changeLayout();

        super.onResize();
    }

    _updateDMState = () => {
        const { room } = this.state;
        if (room.getMyMembership() !== "join") {
            return;
        }
        this._checkIsDirectChat(room);
        super._updateDMState();
    }

    _changeLayout() {
        const { useStraitLayout } = this.state;
        const needUseStraitLayout = this._timelineWidthWatcher.current && this._timelineWidthWatcher.current.offsetWidth < 848;

        if ((useStraitLayout && !needUseStraitLayout) || (!useStraitLayout && needUseStraitLayout)) {
            this.setState({ useStraitLayout: needUseStraitLayout });
        }
    }

    _checkIsDirectChat(room) {
        this.setState({ directChat: checkIsDirectChat(room) });
    }

    _checkIsBridgedChat(room) {
        this.setState({ isBridged: checkIsBridgedChat(room) });
    }

    _preventContextMenu(e) {
        e.preventDefault();
    }

    render() {
        const RoomHeader = sdk.getComponent('rooms.RoomHeader');
        const ForwardMessage = sdk.getComponent("rooms.ForwardMessage");
        const AuxPanel = sdk.getComponent("rooms.AuxPanel");
        const SearchBar = sdk.getComponent("rooms.SearchBar");
        const PinnedEventsPanel = sdk.getComponent("rooms.PinnedEventsPanel");
        const ScrollPanel = sdk.getComponent("structures.ScrollPanel");
        const TintableSvg = sdk.getComponent("elements.TintableSvg");
        const RoomPreviewBar = sdk.getComponent("rooms.RoomPreviewBar");
        const TimelinePanel = sdk.getComponent("structures.TimelinePanel");
        const RoomUpgradeWarningBar = sdk.getComponent("rooms.RoomUpgradeWarningBar");
        const RoomRecoveryReminder = sdk.getComponent("rooms.RoomRecoveryReminder");
        const ErrorBoundary = sdk.getComponent("elements.ErrorBoundary");

        if (!this.state.room) {
            const loading = this.state.roomLoading || this.state.peekLoading;
            if (loading) {
                return (
                    <div className="mx_RoomView">
                        <ErrorBoundary>
                            <RoomPreviewBar
                                canPreview={false}
                                previewLoading={this.state.peekLoading}
                                error={this.state.roomLoadError}
                                loading={loading}
                                joining={this.state.joining}
                                oobData={this.props.oobData}
                            />
                        </ErrorBoundary>
                    </div>
                );
            } else {
                let inviterName = undefined;
                if (this.props.oobData) {
                    inviterName = this.props.oobData.inviterName;
                }
                let invitedEmail = undefined;
                if (this.props.thirdPartyInvite) {
                    invitedEmail = this.props.thirdPartyInvite.invitedEmail;
                }

                // We have no room object for this room, only the ID.
                // We've got to this room by following a link, possibly a third party invite.
                const roomAlias = this.state.roomAlias;
                return (
                    <div className="mx_RoomView">
                        <ErrorBoundary>
                            <RoomPreviewBar onJoinClick={this.onJoinButtonClicked}
                                            onForgetClick={this.onForgetClick}
                                            onRejectClick={this.onRejectThreepidInviteButtonClicked}
                                            canPreview={false}
                                            error={this.state.roomLoadError}
                                            roomAlias={roomAlias}
                                            joining={this.state.joining}
                                            inviterName={inviterName}
                                            invitedEmail={invitedEmail}
                                            oobData={this.props.oobData}
                                            signUrl={this.props.thirdPartyInvite ? this.props.thirdPartyInvite.inviteSignUrl : null}
                                            room={this.state.room}
                            />
                        </ErrorBoundary>
                    </div>
                );
            }
        }

        const myMembership = this.state.room.getMyMembership();
        if (myMembership == 'invite') {
            if (this.state.joining || this.state.rejecting) {
                return (
                    <ErrorBoundary>
                        <RoomPreviewBar
                            canPreview={false}
                            error={this.state.roomLoadError}
                            joining={this.state.joining}
                            rejecting={this.state.rejecting}
                        />
                    </ErrorBoundary>
                );
            } else {
                const myUserId = this.context.credentials.userId;
                const myMember = this.state.room.getMember(myUserId);
                const inviteEvent = myMember ? myMember.events.member : null;
                let inviterName = _t("Unknown");
                if (inviteEvent) {
                    inviterName = inviteEvent.sender ? inviteEvent.sender.name : inviteEvent.getSender();
                }
                // We deliberately don't try to peek into invites, even if we have permission to peek
                // as they could be a spam vector.
                // XXX: in future we could give the option of a 'Preview' button which lets them view anyway.

                // We have a regular invite for this room.
                return (
                    <div className="mx_RoomView">
                        <ErrorBoundary>
                            <RoomPreviewBar
                                onJoinClick={this.onJoinButtonClicked}
                                onForgetClick={this.onForgetClick}
                                onRejectClick={this.onRejectButtonClicked}
                                onRejectAndIgnoreClick={this.onRejectAndIgnoreClick}
                                inviterName={inviterName}
                                canPreview={false}
                                joining={this.state.joining}
                                room={this.state.room}
                            />
                        </ErrorBoundary>
                    </div>
                );
            }
        }

        // We have successfully loaded this room, and are not previewing.
        // Display the "normal" room view.

        const call = this._getCallForRoom();
        let inCall = false;
        if (call && (this.state.callState !== 'ended' && this.state.callState !== 'ringing')) {
            inCall = true;
        }

        const scrollheader_classes = classNames({
            mx_RoomView_scrollheader: true,
        });

        let statusBar;
        let isStatusAreaExpanded = true;

        if (ContentMessages.sharedInstance().getCurrentUploads().length > 0) {
            const UploadBar = sdk.getComponent('structures.UploadBar');
            statusBar = <UploadBar room={this.state.room} />;
        } else if (!this.state.searchResults) {
            const RoomStatusBar = sdk.getComponent('structures.RoomStatusBar');
            isStatusAreaExpanded = this.state.statusBarVisible;
            statusBar = <RoomStatusBar
                room={this.state.room}
                sentMessageAndIsAlone={this.state.isAlone}
                hasActiveCall={inCall}
                isPeeking={myMembership !== "join"}
                onInviteClick={this.onInviteButtonClick}
                onStopWarningClick={this.onStopAloneWarningClick}
                onVisible={this.onStatusBarVisible}
                onHidden={this.onStatusBarHidden}
            />;
        }

        const roomVersionRecommendation = this.state.upgradeRecommendation;
        const showRoomUpgradeBar = (
            roomVersionRecommendation &&
            roomVersionRecommendation.needsUpgrade &&
            this.state.room.userMayUpgradeRoom(this.context.credentials.userId)
        );

        const showRoomRecoveryReminder = (
            SettingsStore.getValue("showRoomRecoveryReminder") &&
            this.context.isRoomEncrypted(this.state.room.roomId) &&
            !this.context.getKeyBackupEnabled()
        );

        const hiddenHighlightCount = this._getHiddenHighlightCount();

        let aux = null;
        let previewBar;
        let hideCancel = false;
        let forceHideRightPanel = false;
        if (this.state.forwardingEvent !== null) {
            aux = <ForwardMessage onCancelClick={this.onCancelClick} />;
        } else if (this.state.searching) {
            hideCancel = true; // has own cancel
            aux = <SearchBar searchInProgress={this.state.searchInProgress} onCancelClick={this.onCancelSearchClick} onSearch={this.onSearch} />;
        } else if (showRoomUpgradeBar) {
            aux = <RoomUpgradeWarningBar room={this.state.room} recommendation={roomVersionRecommendation} />;
            hideCancel = true;
        } else if (showRoomRecoveryReminder) {
            aux = <RoomRecoveryReminder onDontAskAgainSet={this.onRoomRecoveryReminderDontAskAgain} />;
            hideCancel = true;
        } else if (this.state.showingPinned) {
            hideCancel = true; // has own cancel
            aux = <PinnedEventsPanel room={this.state.room} onCancelClick={this.onPinnedClick} />;
        } else if (myMembership !== "join") {
            // We do have a room object for this room, but we're not currently in it.
            // We may have a 3rd party invite to it.
            let inviterName;
            if (this.props.oobData) {
                inviterName = this.props.oobData.inviterName;
            }
            let invitedEmail;
            if (this.props.thirdPartyInvite) {
                invitedEmail = this.props.thirdPartyInvite.invitedEmail;
            }
            hideCancel = true;
            previewBar = (
                <RoomPreviewBar onJoinClick={this.onJoinButtonClicked}
                                onForgetClick={this.onForgetClick}
                                onRejectClick={this.onRejectThreepidInviteButtonClicked}
                                joining={this.state.joining}
                                inviterName={inviterName}
                                invitedEmail={invitedEmail}
                                oobData={this.props.oobData}
                                canPreview={this.state.canPeek}
                                room={this.state.room}
                />
            );
            if (!this.state.canPeek) {
                return (
                    <div className="mx_RoomView">
                        { previewBar }
                    </div>
                );
            } else {
                forceHideRightPanel = true;
            }
        } else if (hiddenHighlightCount > 0) {
            aux = (
                <AccessibleButton element="div" className="mx_RoomView_auxPanel_hiddenHighlights"
                                  onClick={this._onHiddenHighlightsClick}>
                    {_t(
                        "You have %(count)s unread notifications in a prior version of this room.",
                        {count: hiddenHighlightCount},
                    )}
                </AccessibleButton>
            );
        }

        const auxPanel = (
            <AuxPanel room={this.state.room}
                      fullHeight={false}
                      userId={this.context.credentials.userId}
                      conferenceHandler={this.props.ConferenceHandler}
                      draggingFile={this.state.draggingFile}
                      displayConfCallNotification={this.state.displayConfCallNotification}
                      maxHeight={this.state.auxPanelMaxHeight}
                      showApps={this.state.showApps}
                      hideAppsDrawer={false} >
                { aux }
            </AuxPanel>
        );

        let messageComposer; let searchInfo;
        const canSpeak = (
            // joined and not showing search results
            myMembership === 'join' && !this.state.searchResults
        );
        if (canSpeak) {
            const MessageComposer = sdk.getComponent('rooms.MessageComposer');
            messageComposer =
                <MessageComposer
                    room={this.state.room}
                    jumpToBottom={this.jumpToLiveTimeline}
                    callState={this.state.callState}
                    disabled={this.props.disabled}
                    showApps={this.state.showApps}
                    e2eStatus={this.state.e2eStatus}
                    isBridged={this.state.isBridged}
                    permalinkCreator={this._getPermalinkCreatorForRoom(this.state.room)}
                />;
        }

        // TODO: Why aren't we storing the term/scope/count in this format
        // in this.state if this is what RoomHeader desires?
        if (this.state.searchResults) {
            searchInfo = {
                searchTerm: this.state.searchTerm,
                searchScope: this.state.searchScope,
                searchCount: this.state.searchResults.count,
            };
        }

        if (inCall) {
            let zoomButton; let voiceMuteButton; let videoMuteButton;

            if (call.type === "video") {
                zoomButton = (
                    <div className="mx_RoomView_voipButton" onClick={this.onFullscreenClick} title={_t("Fill screen")}>
                        <TintableSvg src={require("matrix-react-sdk/res/img/fullscreen.svg")} width="29" height="22" style={{ marginTop: 1, marginRight: 4 }} />
                    </div>
                );

                videoMuteButton =
                    <div className="mx_RoomView_voipButton" onClick={this.onMuteVideoClick}>
                        <TintableSvg src={call.isLocalVideoMuted() ? require("matrix-react-sdk/res/img/video-unmute.svg") : require("matrix-react-sdk/res/img/video-mute.svg")}
                                     alt={call.isLocalVideoMuted() ? _t("Click to unmute video") : _t("Click to mute video")}
                                     width="31" height="27" />
                    </div>;
            }
            voiceMuteButton =
                <div className="mx_RoomView_voipButton" onClick={this.onMuteAudioClick}>
                    <TintableSvg src={call.isMicrophoneMuted() ? require("matrix-react-sdk/res/img/voice-unmute.svg") : require("matrix-react-sdk/res/img/voice-mute.svg")}
                                 alt={call.isMicrophoneMuted() ? _t("Click to unmute audio") : _t("Click to mute audio")}
                                 width="21" height="26" />
                </div>;

            // wrap the existing status bar into a 'callStatusBar' which adds more knobs.
            statusBar =
                <div className="mx_RoomView_callStatusBar">
                    { voiceMuteButton }
                    { videoMuteButton }
                    { zoomButton }
                    { statusBar }
                    <TintableSvg className="mx_RoomView_voipChevron" src={require("matrix-react-sdk/res/img/voip-chevron.svg")} width="22" height="17" />
                </div>;
        }

        // if we have search results, we keep the messagepanel (so that it preserves its
        // scroll state), but hide it.
        let searchResultsPanel;
        let hideMessagePanel = false;

        if (this.state.searchResults) {
            // show searching spinner
            if (this.state.searchResults.results === undefined) {
                searchResultsPanel = (<div className="mx_RoomView_messagePanel mx_RoomView_messagePanelSearchSpinner" />);
            } else {
                searchResultsPanel = (
                    <ScrollPanel ref={this._searchResultsPanel}
                                 className="mx_RoomView_messagePanel mx_RoomView_searchResultsPanel"
                                 onFillRequest={this.onSearchResultsFillRequest}
                                 resizeNotifier={this.props.resizeNotifier}
                    >
                        <li className={scrollheader_classes}></li>
                        { this.getSearchResultTiles() }
                    </ScrollPanel>
                );
            }
            hideMessagePanel = true;
        }

        const shouldHighlight = this.state.isInitialEventHighlighted;
        let highlightedEventId = null;
        if (this.state.forwardingEvent) {
            highlightedEventId = this.state.forwardingEvent.getId();
        } else if (shouldHighlight) {
            highlightedEventId = this.state.initialEventId;
        }

        const { useStraitLayout, directChat } = this.state;
        const timelineClassNames = classNames({
            "nv_RoomView_messagePanel_useStraitLayout": useStraitLayout,
            "nv_RoomView_direct": directChat,
            "mx_RoomView_messagePanel": true,
        });

        const messagePanel = (
            <TimelinePanel
                ref={this._gatherTimelinePanelRef}
                timelineSet={this.state.room.getUnfilteredTimelineSet()}
                showReadReceipts={this.state.showReadReceipts}
                manageReadReceipts={!this.state.isPeeking}
                manageReadMarkers={!this.state.isPeeking}
                hidden={hideMessagePanel}
                highlightedEventId={highlightedEventId}
                eventId={this.state.initialEventId}
                eventPixelOffset={this.state.initialEventPixelOffset}
                onScroll={this.onMessageListScroll}
                onReadMarkerUpdated={this._updateTopUnreadMessagesBar}
                showUrlPreview = {this.state.showUrlPreview}
                className={timelineClassNames}
                membersLoaded={this.state.membersLoaded}
                permalinkCreator={this._getPermalinkCreatorForRoom(this.state.room)}
                resizeNotifier={this.props.resizeNotifier}
                showReactions={true}
                isBridged={this.state.isBridged}
                directChat={directChat}
            />);

        let topUnreadMessagesBar = null;
        // Do not show TopUnreadMessagesBar if we have search results showing, it makes no sense
        if (this.state.showTopUnreadMessagesBar && !this.state.searchResults) {
            const TopUnreadMessagesBar = sdk.getComponent('rooms.TopUnreadMessagesBar');
            topUnreadMessagesBar = (<TopUnreadMessagesBar
                onScrollUpClick={this.jumpToReadMarker}
                onCloseClick={this.forgetReadMarker}
            />);
        }
        let jumpToBottom;
        // Do not show JumpToBottomButton if we have search results showing, it makes no sense
        if (!this.state.atEndOfLiveTimeline && !this.state.searchResults) {
            const JumpToBottomButton = sdk.getComponent('rooms.JumpToBottomButton');
            jumpToBottom = (<JumpToBottomButton
                numUnreadMessages={this.state.numUnreadMessages}
                onScrollToBottomClick={this.jumpToLiveTimeline}
            />);
        }
        const statusBarAreaClass = classNames(
            "mx_RoomView_statusArea",
            {
                "mx_RoomView_statusArea_expanded": isStatusAreaExpanded,
            },
        );

        const fadableSectionClasses = classNames(
            "mx_RoomView_body", "mx_fadable",
            {
                "mx_fadable_faded": this.props.disabled,
            },
        );

        const showRightPanel = !forceHideRightPanel && this.state.room && this.state.showRightPanel;
        const rightPanel = showRightPanel
            ? <RightPanel roomId={this.state.room.roomId} resizeNotifier={this.props.resizeNotifier} />
            : null;

        const timelineClasses = classNames("mx_RoomView_timeline", {
            mx_RoomView_timeline_rr_enabled: this.state.showReadReceipts,
        });

        const mainClasses = classNames("mx_RoomView", {
            mx_RoomView_inCall: inCall,
        });

        return (
            <RoomContext.Provider value={this.state}>
                <main className={mainClasses} ref={this._roomView} onKeyDown={this.onReactKeyDown}>
                    <ErrorBoundary>
                        <RoomHeader
                            room={this.state.room}
                            searchInfo={searchInfo}
                            oobData={this.props.oobData}
                            inRoom={myMembership === 'join'}
                            onSearchClick={this.onSearchClick}
                            onSettingsClick={this.onSettingsClick}
                            onPinnedClick={this.onPinnedClick}
                            onCancelClick={(aux && !hideCancel) ? this.onCancelClick : null}
                            onForgetClick={(myMembership === "leave") ? this.onForgetClick : null}
                            onLeaveClick={(myMembership === "join") ? this.onLeaveClick : null}
                            e2eStatus={this.state.e2eStatus}
                            isBridged={this.state.isBridged}
                        />
                        <MainSplit
                            panel={rightPanel}
                            resizeNotifier={this.props.resizeNotifier}
                        >
                            <div className={fadableSectionClasses}>
                                {auxPanel}
                                <div ref={this._timelineWidthWatcher} />
                                <div
                                    className={timelineClasses}
                                    onContextMenu={this._preventContextMenu}
                                >
                                    {topUnreadMessagesBar}
                                    {jumpToBottom}
                                    {messagePanel}
                                    {searchResultsPanel}
                                </div>
                                <div className={statusBarAreaClass}>
                                    <div className="mx_RoomView_statusAreaBox">
                                        <div className="mx_RoomView_statusAreaBox_line" />
                                        {statusBar}
                                    </div>
                                </div>
                                {previewBar}
                                {messageComposer}
                            </div>
                        </MainSplit>
                    </ErrorBoundary>
                </main>
            </RoomContext.Provider>
        );
    }
}
