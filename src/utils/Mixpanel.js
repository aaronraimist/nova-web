import mixpanel from 'mixpanel-browser';
import SdkConfig from "matrix-react-sdk/src/SdkConfig";

// TODO refactor
const token = SdkConfig.get().mixpanel_token;

let ready = false;
if (token) {
    mixpanel.init(token, { persistence: 'localStorage' });
    console.log('Mixpanel was inited.');
    ready = true;

    // was bug with tracking and Mixpanel will identify users after they relogin
    // so need force identify users
    // this code will be remove on next commit
    // TODO remove this code after Mixpanel identify all users
    if (localStorage && localStorage.getItem('mx_user_id')) {
        mixpanel.identify(localStorage.getItem('mx_user_id'));
    }
} else {
    console.log('Mixpanel token not defined.');
}

const actions = {
    identify: (id) => mixpanel.identify(id),
    track: (name, props) => {
        if (!ready) return;
        mixpanel.track(name, props);
    },
    reset: () => mixpanel.reset(),
    register: (props) => mixpanel.register(props),
    register_once: (props) => mixpanel.register_once(props),
    people: {
        set: (props) => mixpanel.people.set(props),
        increment: (props) => mixpanel.people.increment(props),
    },
};

const customActions = {
    identifyLogin: (userId) => {
        if (!ready) return;
        actions.identify(userId);
        actions.track('Login');
        actions.people.set({ $name: userId });
    },
    logout: () => {
        if (!ready) return;
        actions.track('Logout');
        actions.reset();
    },
};

export default { ...actions, ...customActions };
