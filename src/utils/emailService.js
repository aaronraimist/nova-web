import { MatrixClientPeg } from 'matrix-react-sdk/src/MatrixClientPeg';

// const APP_PROTOCOL = `novachat://`;
const BASE_URL = 'https://nova.gitlab.io/nova-web';

const _getEmailHtml = (sender, text, ts, link) => {
    return [
        `<p>Sender: ${sender}</p>`,
        `<p>Date: ${new Date(ts)}</p>`,
        `<p><a href="${link}">Open in browser</a></p>`,
        `<p>Message: ${text}</p>`,
    ] .join('');
};

// const _getElectronLink = () => {
//     const { protocol, href } = window.location;
//     if (APP_PROTOCOL === protocol) {
//         return href;
//     }
//     const [, path] = href.split('/#/');
//
//     return `${APP_PROTOCOL}#/${path}`;
// };

const _getWebLink = (roomId, messageId) => {
    return `${BASE_URL}/#/room/${roomId}/${messageId}`;
};

const _sanitizeString = (str) => {
    return str
        .replace(/<del>(.*?)<\/del>|_/gim, '$1')
        .replace(/([^\p{Alpha}\p{M}\p{Nd}\p{Pc}\p{Join_C}])/gu, ' ')
        .replace(/[ ]{2,}/g, ' ')
        .trim();
};

const _getDisplayUserName = (userId) => {
    const cli = MatrixClientPeg.get();
    const user = cli.getUser(userId) || { displayName: userId };
    return user.displayName;
};

export const sendEmail = async (endpointUrl, email, event) => {
    const { sender: userId, content, origin_server_ts: ts, event_id: eventId, room_id: roomId } = event;
    const to = email;
    const subject = content.body.substring(0, 101);
    const text = content.formatted_body ? content.formatted_body : content.body;
    const senderUsername = _getDisplayUserName(userId);
    const link = _getWebLink(roomId, eventId);

    const message = {
        subject: _sanitizeString(subject),
        to: to,
        html: _getEmailHtml(senderUsername, text, ts, link),
    };
    try {
        await fetch(endpointUrl, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
            },
            body: JSON.stringify(message),
        });
    } catch (e) {
        console.log(e);
    }
};
